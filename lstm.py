# train a lstm model for anomaly detection

import pandas as pd 
import numpy as np 
 
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from keras.callbacks import EarlyStopping

# from pyspark import SparkContext, SparkConf

# from elephas.utils.rdd_utils import to_simple_rdd
# from elephas.spark_model import SparkModel
# from elephas import optimizers as elephas_optimizers

# hyper-parameters
sequence_length = 9
epochs = 50
batch_size = 1024

def create_model(sequence_length, layers):
    model = Sequential()
    model.add(LSTM(layers['hidden1'],
                   input_shape=(sequence_length - 1, layers['input']),
                   return_sequences=True))
    model.add(Dropout(0.2))

    model.add(LSTM(layers['hidden2'], return_sequences=True))
    model.add(Dropout(0.2))

    model.add(LSTM(layers['hidden3'], return_sequences=False))
    model.add(Dropout(0.2))

    model.add(Dense(layers['output']))
    model.add(Activation("linear"))

    model.compile(loss="mse", optimizer="rmsprop")
    return model


def run():
	# load prepared train data
	data = pd.read_csv('postures_train.csv',header=None)
	train = np.array(data)
	X_train = train[:,:-1]
	y_train = train[:,-1]
	X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))

	# spark conf
	# conf = SparkConf().setAppName('Elephas_App').setMaster('local[8]')
	# sc = SparkContext(conf=conf)

	# create RDD from train dataset
	# rdd = to_simple_rdd(sc, X_train, y_train)

	# define the model 
	# LSTM layers
	layers = {
	    'input': 1,
	    'hidden1': 64,
	    'hidden2': 256,
	    'hidden3': 100,
	    'output': 1
	}
	model = create_model(sequence_length=sequence_length, layers=layers)

	# train the model
	print("start to train the model....")
	# rms = elephas_optimizers.RMSprop()
	# adagrad = elephas_optimizers.Adagrad()
	early_stopping = EarlyStopping(monitor='val_loss', patience=3)
	# spark_model = SparkModel(sc,model, optimizer=adagrad, frequency='epoch', mode='asynchronous', num_workers=2)
	# spark_model.train(rdd, nb_epoch=epochs, batch_size=batch_size, verbose=0, validation_split=0.1)
	model.fit(X_train, y_train, epochs=epochs, batch_size=batch_size, validation_split=0.1, callbacks=[early_stopping])

	# save the model to disk 
	# serialize model to JSON
	model_json = model.to_json()
	with open("tf_models/lstm_model.json", "w") as json_file:
	    json_file.write(model_json)
	# serialize weights to HDF5
	model.save_weights("tf_models/lstm_model.h5")
	print("Saved model to disk")

if __name__ == '__main__':
	run()

