# This is the source code repo for the guest lecture on anomaly detection

## It contains the following scripts:

#### The python notebook file for one-class SVM
#### The python notebook file for isolation forest
#### They python script for training the LSTM model